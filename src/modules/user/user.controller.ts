import { NextFunction, Request, Response } from "express";
import { formatError } from "../../utils/formatError";
import { formatResponse } from "../../utils/formatResponse";
import { CreateUserDTO } from "./user.DTO";
import { User } from "./user.model";
import { createUserService, getUserByIdService, getUsersService } from "./user.service";

// TODO i18n

const getUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    const allUsers = await getUsersService();
    const response = formatResponse("Users founded", allUsers);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json("Internal server error");
  }
};

const getUserById = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    const userId= req.params.id
    const userData = await getUserByIdService(userId);
    const response = formatResponse("Users founded", userData);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json("Internal server error");
  }
};

const getUserDetail = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    const userId= "SE TOMARA DEL TOKEN"
    const userDetail = await getUserByIdService(userId);
    const response = formatResponse("Users founded", userDetail);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json("Internal server error");
  }
};

const createUser = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<Response> => {
  try {
    const {
      firstName,
      lastName,
      email,
      password,
      phone,
      image,
      status,
      category,
      firstLogin,
      team,
      points,
      role,
    }: CreateUserDTO = req.body;
    await createUserService({
      firstName,
      lastName,
      email,
      password,
      phone,
      image,
      status,
      category,
      firstLogin,
      team,
      points,
      role,
    });
    const response = formatResponse("User created", {});
    return res.status(201).json(response);
    
  } catch (error) {
    console.log("🚀 ~ file: user.controller.ts:38 ~ error", error);
    return res.status(500).json("Internal server error: ");
  }
};

export { getUsers, getUserById, getUserDetail, createUser };
