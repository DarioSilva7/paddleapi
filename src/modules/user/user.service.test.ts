import { ValidationError } from "sequelize";
import {
  bulkCreateUserFixture,
  CreateUserServiceFixture,
} from "../../test/fixtures/user.fixtures";
import { User } from "./user.model";
import {
  createUserService,
  getUserByIdService,
  getUsersService,
  updateUserService,
} from "./user.service";
import request from "supertest";
import app from "../../app";
import { v4 as uuidv4 } from "uuid";

describe("USER SERVICE", () => {
  beforeEach(async () => {
    try {
      await User.bulkCreate(bulkCreateUserFixture);
      console.log("🚀 ~ file: user.service.test.ts ~ beforeEach ~ User.create");
      return;
    } catch (error) {
      console.log(
        "🚀 ~ file: user.service.test.ts:16 ~ beforeEach ~ error",
        error
      );
    }
  });

  afterEach(async () => {
    await User.destroy({ force: true, where: {} });
    console.log("🚀 ~ file: user.service.test.ts ~ afterEach ~ User.destroy ");
    return;
  });

  describe("getUsersService - Happy path", () => {
    test("Should return the users", async () => {
      const response = await getUsersService();
      expect(response.length).toBe(2);
    });
  });
  //   describe("getUsersService - UnHappy path", () => {
  //     test("Should not return the users ", () => {});
  //   });
  describe("createUserService - Happy path", () => {
    test("Should create the user", async () => {
      const rta = await createUserService(CreateUserServiceFixture);
      expect(rta).toBe(true);
    });
  });
  describe("createUserService - UnHappy path", () => {
    const validModel = {
      firstName: "valid name",
      lastName: "valid lastName",
      email: "validmail@mail.com",
      password: "valid qwerty",
      phone: "valid 123456788",
      image: "valid an image",
      status: "ACTIVE",
      category: "19cd566d-6a41-43df-9df9-38bffc39db66",
      firstLogin: true,
      team: "19cd566d-6a41-43df-9df9-38bffc39db55",
      points: "23",
      role: "PLAYER",
    };
    const requiredValues = [
      "firstName",
      "lastName",
      "email",
      "password",
      "phone",
      "category",
    ];

    for (const attr of requiredValues) {
      const { [attr as keyof typeof validModel]: removed, ...values } =
        validModel;

      test(`Can't create user without ${attr}`, () => {
        expect(async () => {
          //@ts-ignore
          const algo = await createUserService(values);
        }).rejects.toThrow(ValidationError);
      });
    }
  });
  describe("getUserByIdService - Happy path", () => {
    test("Should return the user details", async () => {
      const userCreated = await User.create({
        firstName: "detail Name",
        lastName: "detail lastName",
        email: "detail@test.com",
        password: "qwerty",
        phone: "001345679",
        image: "an image",
        category: "19cd566d-6a41-43df-9df9-38bffc39db66",
      });

      const userDetail = await getUserByIdService(userCreated.id);
      expect(userDetail).toBeDefined();
      expect(userDetail?.firstName).toBe(userCreated.firstName);
    });
    test("Should not return the user details if the id do not exist", () => {
      expect(async () => {
        await getUserByIdService(uuidv4());
      }).rejects.toThrowError("User not found");
    });
  });
  describe("updateUserService - Happy path", () => {
    test("Should update user data", async () => {
      const userFound = await User.findOne({
        where: { email: bulkCreateUserFixture[0].email },
      });
      if (userFound) {
        await updateUserService(userFound.id, { firstName: "Updated" });
        const userUpdated = await User.findOne({
          where: { email: bulkCreateUserFixture[0].email },
        });
        expect(userUpdated?.firstName).toBe("Updated");
      }
    });
  });
  describe("updateUserService - UnHappy path", () => {
    test("Should not update user data if the id do not exist", () => {
      expect(async () => {
        await updateUserService(uuidv4(), { firstName: "Not update" });
      }).rejects.toThrowError("An error ocurred trying to update");
    });
  });
});
