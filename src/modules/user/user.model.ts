import { DataTypes, Model } from "sequelize";
import { db } from "../../config/database";
import { v4 as uuidv4 } from "uuid";
import { BaseUserModel, USER_ROLE, USER_STATUS } from "./user.interface";
import {
  USER_MODEL_VALIDATIONS_ALPHANUMERIC,
  USER_MODEL_VALIDATIONS_LEN,
} from "../../constants/errors";

class User extends Model implements BaseUserModel {
  id!: string;
  firstName!: string;
  lastName!: string;
  email!: string;
  password!: string;
  phone!: string;
  image!: string;
  status!: string;
  category!: string;
  firstLogin!: boolean;
  team!: string;
  points!: string;
  role!: string;
}

User.init(
  {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      validate: {
        isUUID: { args: 4, msg: "Id must to be a UUID" },
      },
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: "First name must not be null" },
        is: {
          args: /^[a-zA-Z0-9\u00C0-\u017F\s\.]+$/,
          msg: USER_MODEL_VALIDATIONS_ALPHANUMERIC.firstName,
        },
        len: {
          args: [3, 15],
          msg: USER_MODEL_VALIDATIONS_LEN.firstName,
        },
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: "Last name must not be null" }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: { msg: "It's an invalid email" },
        notNull: { msg: "Email must not be null" },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: "Password must not be null" }
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: { msg: "Phone must not be null" }
      }
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    status: {
      type: DataTypes.ENUM(...Object.keys(USER_STATUS)),
      defaultValue: USER_STATUS.ACTIVE,
      validate: {
        isIn: {
          args: [[...Object.keys(USER_STATUS)]],
          msg: "User status must be one of ACTIVE or INACTIVE"
        }
      }
    },
    category: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    firstLogin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    team: {
      type: DataTypes.UUID,
      allowNull: true,
    },
    points:{
      type: DataTypes.STRING,
      allowNull: true
    },
    role: {
      type: DataTypes.ENUM(...Object.keys(USER_ROLE)),
      defaultValue: USER_ROLE.PLAYER,
    },
  },
  {
    sequelize: db.sequelize,
    modelName: "user",
  }
);

User.beforeCreate(async (user) => {
  user.id = uuidv4();
});

export { User };
