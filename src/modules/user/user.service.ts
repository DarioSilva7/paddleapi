import {
  CreateUserDTO,
  UpdateUserDTO,
  UserAttrI,
  UserCreatedI,
  UserDetailI,
} from "./user.DTO";
import { User } from "./user.model";

const getUsersService = async (): Promise<UserAttrI[]> => {
  const allUsers = await User.findAll();
  return allUsers;
};

const getUserByIdService = async (id: string): Promise<UserDetailI | null> => {
  const userData = await User.findByPk(id);
  if (!userData) {
    throw new Error("User not found");
  }
  const userDetail = {
    firstName: userData.firstName,
    lastName: userData.lastName,
    email: userData.email,
    phone: userData.phone,
    image: userData.image,
    category: userData.category,
    team: userData.team,
    points: userData.points,
  };
  return userDetail;
};

const createUserService = async (userData: CreateUserDTO): Promise<boolean> => {
  await User.create(userData);
  return true;
};

const updateUserService = async (userId: string, userData: UpdateUserDTO): Promise<boolean | null> => {
  const userFound = await User.findOne( { where: { id : userId }});
  if(userFound){
    userFound.set(userData)
    await userFound.save()
    return true;
  }
  throw new Error("An error ocurred trying to update");
  
};

export { getUsersService, createUserService, getUserByIdService, updateUserService };
