import { BaseUserModel } from "./user.interface";
import { User } from "./user.model";

// export interface UserModelI extends BaseUserModel{}

export interface UserAttrI extends BaseUserModel{}

export interface CreateUserDTO extends Omit<BaseUserModel, 'id'>{}
export interface UserCreatedI extends Partial<User>{}
export interface UserDetailI extends Omit<UserAttrI, "id" | "password" | "status" | "firstLogin" | "role" >{}

export interface UpdateUserDTO extends Partial<CreateUserDTO>{}