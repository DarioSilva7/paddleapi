export interface BaseUserModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  image: string;
  status: string;
  category: string;
  firstLogin: boolean;
  team: string;
  points: string;
  role: string;
}

export const USER_STATUS= {
  ACTIVE: "ACTIVE",
  INACTIVE: "INACTIVE"
}

export const USER_ROLE= {
  GUEST: "GUEST",
  PLAYER: "PLAYER",
  ADMIN: "ADMIN"
}