import { CreateUserDTO, UpdateUserDTO, UserAttrI } from "../../modules/user/user.DTO";
import { v4 as uuidv4 } from "uuid";
import { USER_ROLE, USER_STATUS } from "../../modules/user/user.interface";

export const bulkCreateUserFixture = [
  {
    id: uuidv4(),
    firstName: "bulk firstName",
    lastName: "bulk lastName",
    email: "bulk@test.com",
    password: "qwerty",
    phone: "1234567",
    image: "an image",
    status: USER_STATUS.ACTIVE,
    category: "19cd566d-6a41-43df-9df9-38bffc39db66",
    firstLogin: true,
    team: "19cd566d-6a41-43df-9df9-38bffc39db55",
    points: "23",
    role: USER_ROLE.PLAYER,
  },
  {
    id: uuidv4(),
    firstName: "bulk firstName 2",
    lastName: "bulk lastName 2",
    email: "bulk2@test.com",
    password: "qwerty",
    phone: "12345678",
    image: "an image",
    status: USER_STATUS.ACTIVE,
    category: "19cd566d-6a41-43df-9df9-38bffc39db66",
    firstLogin: true,
    team: "19cd566d-6a41-43df-9df9-38bffc39db55",
    points: "23",
    role: USER_ROLE.PLAYER,
  },
];

export const CreateUserServiceFixture: CreateUserDTO = {
  firstName: "test fristName",
  lastName: "test lastName",
  email: "test@test.com",
  password: "qwerty",
  phone: "12345679",
  image: "an image",
  status: USER_STATUS.ACTIVE,
  category: "19cd566d-6a41-43df-9df9-38bffc39db66",
  firstLogin: true,
  team: "19cd566d-6a41-43df-9df9-38bffc39db55",
  points: "23",
  role: USER_ROLE.PLAYER
};

export const FailCreateUserServiceFixture: UpdateUserDTO =
  {
    firstName: "name",
    lastName: "lastName",
    email: "mail@mail.com",
    password: "qwerty",
    phone: "123456788",
    image: "an image",
    status: USER_STATUS.ACTIVE,
    category: "19cd566d-6a41-43df-9df9-38bffc39db66",
    firstLogin: true,
    team: "19cd566d-6a41-43df-9df9-38bffc39db55",
    points: "23",
    role: USER_ROLE.PLAYER
  }
