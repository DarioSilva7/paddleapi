import { db } from "../config/database";

// el cierre de coneccion a db debe estar en este archivo
// dado que es lo ultimo en ejecutarse luego de todas las suits de tests
const tearDownTests = async () => {
    // eliminamos todas las tablas
    // await db.sequelize.drop()
    // cerramos conexion
    await db.sequelize.close()
};
export default tearDownTests;