export const formatResponse = (message: string, data: any)=>{
    return {
        message,
        data,
        errors: []
    }
}