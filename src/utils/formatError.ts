export const formatError = (message: string, errors: Error)=>{
    return {
        message,
        data:{},
        errors
    }
}