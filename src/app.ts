import express from 'express';
import authRoutes from './modules/auth/auth.routes';
import userRoutes from './modules/user/user.routes';
import swaggerUi from 'swagger-ui-express';
import swaggerSetup from './docs/swagger';
import pingRoutes from './modules/ping/ping.routes';
import i18next from 'i18next';
import backend from 'i18next-fs-backend';
import * as middleware from 'i18next-http-middleware';

i18next
  .use(backend)
  .use(middleware.LanguageDetector)
  .init({
    fallbackLng: 'en', //we set english as default languaje
    backend: {
      loadPath: './locales/{{lng}}/translation.json' //we dinamically read translation file according {{lng}} (languaje) detected
    }
  });

const app = express()
app.use(middleware.handle(i18next)); //we set i18n middleware for all routes

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerSetup));


app.get("/ping", pingRoutes)
app.use('/auth', authRoutes)
app.use('/users', userRoutes)

export= app;