// eslint-disable-next-line @typescript-eslint/no-var-requires
import { readFileSync } from 'fs';
const userStructure = readFileSync(
  __dirname + '/files/user.structure.sql',
  'utf8'
);
const dropUserStructure = readFileSync(
  __dirname + '/files/drop.user.structure.sql',
  'utf8'
);
export function up(queryInterface) {
  return queryInterface.sequelize.query(userStructure);
}
export function down(queryInterface) {
  return queryInterface.sequelize.query(dropUserStructure);
}